var fs = require('graceful-fs');

// combinationsarray = [{first_word, second_word}]
global.add_combination = function(combinationsarray)
{
	fs.readFile('./rs/word_combinations.json', 'utf8', function(err, data) {  
    	if (err) throw err;

    	var words_json = JSON.parse(data).word_combinations;

    	for(k=0; k< combinationsarray.length; k++){
    		var first_word = combinationsarray[k].first_word;
    		var second_word = combinationsarray[k].second_word;
	    	var not_found = true;
	    	var message = '';

	    	for(i=0; i<words_json.length; i++){
	    		if(words_json[i].first_word == first_word && words_json[i].second_word == second_word){
	    			words_json[i].occurences++;
	    			not_found = false;
	    			message = 'updated combination of ' + first_word + ' and ' + second_word + " with one more occurence. \n" + words_json[i].occurences + " occurences in total";
	    			break;
	    		}
	    	}

	    	if(not_found){
	    		words_json.push({
	    			"first_word": first_word,
	    			"second_word" : second_word,
	    			"occurences" : 1
	    		});
	    		message = "created new occurence of " + first_word + " and " + second_word;
	    	}

	    	var word_combinations = {
	    		"word_combinations": words_json
	    	}

	    	console.log(message);
	    }
    	fs.writeFile('./rs/word_combinations.json', JSON.stringify(word_combinations, null, 4), function (err) {
		  if (err) throw err;
		  console.log('Database Updated.');
		});
	});
}

global.add_first_word = function(wordsarray)
{
	fs.readFile('./rs/start_words.json', 'utf8', function(err, data) {  
    	if (err) throw err;

    	var words_json = JSON.parse(data).start_words;

    	for(k=0; k< wordsarray.length; k++){
    		var word = wordsarray[k];
	    	var not_found = true;
	    	var message = '';

	    	for(i=0; i<words_json.length; i++){
	    		if(words_json[i].word == word){
	    			words_json[i].occurences++;
	    			not_found = false;
	    			message = 'updated occurence of ' + word + " with one more occurence. \n" + words_json[i].occurences + " occurences in total";;
	    			break;
	    		}
	    	}

	    	if(not_found){
	    		words_json.push({
	    			"word": word,
	    			"occurences" : 1
	    		});
	    		message = "created new occurence of " + word;
	    	}

	    	var start_words = {
	    		"start_words": words_json
	    	}

	    	console.log(message);
    	}

    	fs.writeFile('./rs/start_words.json', JSON.stringify(start_words, null, 4), function (err) {
		  if (err) throw err;
		});

	});
}

global.parse_fb = function(json_d, cb) // parses thread json provided by facebook and callsback the array with messages or null if it fails
{
	var messages_array = [];

	fs.readFile(json_d, 'utf8', function(err, data) 
	{  
    	if (err){ cb(null); }
    	console.log("Analyzing json.");

    	var facebook_json = JSON.parse(data).messages;

    	for(var i = 0; i < facebook_json.length; i++)
    	{
    		messages_array.push(facebook_json[i].message);
    	}
    	
    	console.log("Done analyzing json."); cb(messages_array);
	});
}

global.bidzerify = function(msg_arr)
{
	var message_array = [];
	var start_message = [];

	for(var i = 0; i < msg_arr.length; i++)
	{
		var msg = msg_arr[i].replace(/[^\w\s]/g,'').replace(/:.*:/, '').toLowerCase().split(' ');

		if(msg[0] != 'image' && msg[0] != ' ' && msg[0] != '')
		{
			console.log('Message N: '+ i);

			for(var r = 0; r < msg.length; r++)
			{
				if(r == msg.length-1) break;
				
				var msg_obj = 
				{
					first_word: msg[r],
					second_word: msg[r+1]
				};

				message_array.push(msg_obj);
				start_message.push(msg[0]);
			}
		}
	}

	add_combination(message_array);
	add_first_word(start_message);
}

global.respond_message = function(length, start, cb)
{
	var unzipped_words = [];
	var unzipped_word_combinations = [];
	var response = start ? ' ' + start : '';
	var word_chain = start ? start : '';

	fs.readFile('./rs/start_words.json', 'utf8', function(err, data) 
	{  
    	if (err){ cb(null); }

    	var facebook_json = JSON.parse(data).start_words;

    	if(word_chain == '')
    	{
	    	for(var i = 0; i < facebook_json.length; i++)
	    	{
	    		for(var r = 0; r < facebook_json[i].occurences; r++)
	    		{
	    			unzipped_words.push(facebook_json[i].word);
	    		}
	    	}
	    	
	    	var rand_resp = random_(unzipped_words.length, 0);
	    	response += ' ' + unzipped_words[rand_resp];
	    	word_chain = unzipped_words[rand_resp];
    	}
    	else
    	{
	    	for(var i = 0; i < facebook_json.length; i++)
	    	{
	    		for(var r = 0; r < facebook_json[i].occurences; r++)
	    		{
	    			unzipped_words.push(facebook_json[i].word);
	    		}
	    	}    		
    	}

		fs.readFile('./rs/word_combinations.json', 'utf8', function(err, data)
		{
	    	if (err){ cb(null); }

	    	var json_resp = JSON.parse(data).word_combinations;

	    	for(var r = 0; r < length; r++)
	    	{
				for(var n = 0; n < json_resp.length; n++)
				{
					if(json_resp[n].first_word == word_chain)
					{
						for(var z = 0; z < json_resp[n].occurences; z++)
						{
							unzipped_word_combinations.push(json_resp[n].second_word);							
						}	
					}
				}

				var rand_resp2 = random_(unzipped_word_combinations.length, 0);

				if(unzipped_word_combinations[rand_resp2] == undefined)
				{
					rand_resp = random_(unzipped_words.length, 0);
					response += '. ' + unzipped_words[rand_resp];
					word_chain = unzipped_words[rand_resp];
					unzipped_word_combinations = [];				
				}
				else
				{					
					word_chain = unzipped_word_combinations[rand_resp2];
					response += ' ' + unzipped_word_combinations[rand_resp2];
					unzipped_word_combinations = [];								
				}			
	    	}

	    	cb(response);
    	});
	});
}

global.random_ = function(a, b) // returns random number from a(MAX) to b(MIN)
{
    var rand = Math.floor((Math.random()*a)+b);
    
    return rand;
}//____________________________________________________________________________


