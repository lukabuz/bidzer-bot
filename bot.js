'use strict';

const Discord = require('discord.js');
const cfg = require('./config.js');
const db = require('./rs/db.js');

//____________________________________________________________________________
//init:

const client = new Discord.Client({disableEveryone: true});

//____________________________________________________________________________
//code:


client.on('ready', function()
{
	console.log('------------------');
});

client.on('error', function(err) // web error handling to reconnect
{
	console.log('[ERROR WEBSOCKET]: ' + err);
	client.login(cfg.token);
});

client.on('disconnect', function(err) // disconnected from client, logoff
{
	console.log('[Disconnected]');
});

// Create an event listener for messages
client.on('message', function(message) // message handling
{
	if(message.channel.type != "text") return;
	if(message.author.bot) return;

	var msg = message.content.replace(/[^\w\s]/g,'').replace(/:.*:/, '').toLowerCase().split(' '); // message split on whitespace array
	var msg_array = [];

	if(msg[0] == "bidzer")
	{
		if(msg[1] > 200) return;

		respond_message(msg[1], msg[2], function(respond)
		{
			if(respond == null) return;

			message.channel.send(respond);
			message.react('🎱');
		});
	}
	else if(msg[0] == "garepe")
	{
		respond_message(1, msg[1], function(respond)
		{
			if(respond == null) return;
			var repi = ' ';

			respond_message(15, msg[1], function(response)
			{
				if(respond == null) return;

				for(var k = 0; k < 3; k++)
				{
					for(var b = 0; b < 2; b++)
					{
						for(var x = 0; x < 4; x++)
						{
							repi += respond + ' ay,';
						}
						repi += '\n';				
					}
					repi += '\n';					

					repi += response;
					repi += '\n------------------------------------------------\n\n';	
				}
				message.channel.send(repi);
				message.react('🎱');					
			});
		});		
	}
	else
	{
		for(var i = 0; i < msg.length; i++)
		{
			if(i == msg.length-1) break;
			
			var msg_obj = 
			{
				first_word: msg[i],
				second_word: msg[i+1]
			};

			msg_array.push(msg_obj);
		}

		if(msg.length > 1)
		{
			add_combination(msg_array);
			add_first_word([msg[0]]);					
		}			
	}
});//___________________________________________________________________________

function shutdown() // graceful shutdown
{
	console.log('Shutting down...');
	client.destroy();
	setTimeout(function(){process.exit();}, 1000);
}//____________________________________________________________________________
//start:

process.on('SIGINT', function(){ console.log('Ctrl-C pressed'); shutdown(); });

client.login(cfg.token);








// parse_fb('./rs/thread_49.json', function(cb)
// {
// 	if(cb == null) return;
	
// 	bidzerify(cb);
// });

