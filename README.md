Bidzer Bot

Discord chat bot with learning capabilities.

BASIC IDEA

LEARNING FROM MESSAGES

The bot will be constantly listening to and parsing every message sent in discord chat. On every message(even the ones that dont call to or involve the bot itself), the bot will create an array of words in said message.


example:
	Message from lukabuz: "Hey guys, how are you doing?"

	Bot step 1 - pick out each individual word, sanitize them(remove anything that isnt a letter) and put them into an array.
	
	["hey", "guys", "how", "are", "you", "doing"]

	Bot step 2 - create an array of combinations of words that appeared sequentially in the chat.

	[["hey", "guys"] , ["guys", "how"], ["how", "are"], ["are", "you"], ["you", "doing"]]

	Bot step 3 - see which words the sentence began with, and end with and store them

	var startedwith = "hey";
	var endedwith = "doing";

	Before we move on to step 4, it is important the reader has an idea of the database schema. This Database is provided in the db as a mysql workbench model file and a png.

	Bot step 4 - update the # of occurences of each word combination. If the bot does not find the word combination in the DB, it should add it in with a score of 1. Also do the same for starting and ending words.


When the user asks a bot something, the bot replies with a random number of words(2-10) that are chosen based on the following proccess:
	
example:
	
	Bot is asked to provide a response.

	Bot step 1 - bot chooses a random number from 2-10, this will be the number of words in the sentence.

	Bot step 2 - bot chooses a starting word randomly. But, the chance that a word will be chosen will be higher the higher it has previously occured as a starting word(this will be seen in the database.)

	If the db has the following entries:

	ID - word - occurences
	1 - "hey" - 5
	2 - "rogor" - 3
	3 - "shen" - 4

	The bot will create an array where each word is entered the amount of times it has occured previously, so the array created with the following example would be:

	["hey", "hey", "hey", "hey", "hey", "rogor", "rogor", "rogor", "shen", "shen", "shen", "shen"];

	The bot will then choose a random number from 0 to array.length and the first word of its response would be examplearray[thatrandomnumber]

	Then, the bot will do exactly the same type of random selection, this time on an array found by using the first word to find combinations in the two_word_combinations database. The next word will be the second_word of the chosen combination entry.

	This process will go on until the bot does its last word.

	The bot will return the message, hopefully something that will make some vague sense.

	NOTE: endword is not used at all, but I still chose to implement it as it may be needed in future updates
